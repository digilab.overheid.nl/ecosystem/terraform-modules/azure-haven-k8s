resource "azurerm_dns_zone" "default" {
  name                = var.domain_name
  resource_group_name = azurerm_resource_group.default.name
}

resource "azurerm_dns_a_record" "wildcard" {
  zone_name           = azurerm_dns_zone.default.name
  resource_group_name = azurerm_resource_group.default.name
  name                = "*"
  ttl                 = 300
  records             = length(var.loadbalancer_ips) == 0 ? azurerm_public_ip.ingress_ipv4.*.ip_address : var.loadbalancer_ips
}

resource "azurerm_dns_a_record" "lb" {
  zone_name           = azurerm_dns_zone.default.name
  resource_group_name = azurerm_resource_group.default.name
  name                = "lb"
  ttl                 = 300
  records             = length(var.loadbalancer_ips) == 0 ? azurerm_public_ip.ingress_ipv4.*.ip_address : var.loadbalancer_ips
}

resource "azurerm_dns_a_record" "int_wildcard" {
  count = var.internal_loadbalancer_ip != "" ? 1 : 0

  zone_name           = azurerm_dns_zone.default.name
  resource_group_name = azurerm_resource_group.default.name
  name                = "*.int"
  ttl                 = 300
  records             = [var.internal_loadbalancer_ip]
}
